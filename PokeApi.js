var texto='';
let consultarPokemon=()=>{
    fetch(`https://pokeapi.co/api/v2/pokemon/?limit=1050`)
        .then(response => response.json())
        .then(datos =>{
            let div = document.getElementById("toHTML");
            let count = datos['count'];
            console.log(count)
            for(let i =1; i< count ;i++){
                let url = datos['results'][i]['url'];
                fetch(url)
                .then(response => response.json())
                .then(data =>{
                    let img = data.sprites.front_default;
                    let name = data.name;
                    if(img == null){
                        texto += `
                            <div class="grid-item">
                                <p>no hay imagen disponible</p>
                                <h3 class="overlay">${name}</h3>
                            </div>
                        `;
                    }else{
                        texto += `
                            <div class="grid-item">
                                <img  id="imagen" loading="lazy" src="${img}">
                                <h3 class="overlay">${name}</h3>
                            </div>
                        `;
                    }
                    div.innerHTML = texto;
                })
            }
        })
}

consultarPokemon();